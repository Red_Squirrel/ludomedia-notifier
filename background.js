// Coded by Red Squirrel (@redsquirrel87 - http://www.redsquirrel87.com)
var token = '', id = '';
var update_time = 1;

function parseError(text) {
	// console.error("[Ludomedia] " + text);
	chrome.browserAction.setIcon({path:"not_logged.png"});
	chrome.browserAction.setBadgeBackgroundColor({color:[190, 190, 190, 230]});
	chrome.browserAction.setBadgeText({text:"?"});	
	chrome.browserAction.setTitle({title: chrome.i18n.getMessage("click_to_get_token")});
}

function parseSuccess(result) {
	var obj;
	try {
		obj = JSON.parse(result);     
	} catch (e) {
		parseError("Invalid JSON response: " + result);
		return;
	}
	if (obj && obj.notifiche) {
		var notifiche =		obj.notifiche.notifiche;
		var amici =		obj.notifiche.amici;
		var messaggi =		obj.notifiche.messaggi;
		var matchmaking =	obj.notifiche.matchmaking;
		
		var testo =	chrome.i18n.getMessage("notification")	+ ": " + notifiche	+ "\n" +
				chrome.i18n.getMessage("friends")	+ ": " + amici		+ "\n" +
				chrome.i18n.getMessage("message")	+ ": " + messaggi	+ "\n" +
				chrome.i18n.getMessage("matchmaking")	+ ": " + matchmaking;
		
		var count =	(notifiche ? parseInt(notifiche, 10) : 0) + 
				(amici ? parseInt(amici, 10) : 0) + 
				(messaggi ? parseInt(messaggi, 10) : 0) + 
				(matchmaking ? parseInt(matchmaking, 10) : 0);
				
		chrome.browserAction.setIcon({path: "logged.png"});
		chrome.browserAction.setBadgeBackgroundColor({color:[24, 0, 208, 255]});
		chrome.browserAction.setBadgeText({text: count.toString()});
		chrome.browserAction.setTitle({title: testo});
	} else {
		parseError("No notification object in JSON result!");
	}
}

function getNotifications() {
	//console.log("[Ludomedia] Getting notifications...");
	if (token == '') {
		parseError('No Token!');
		return;
	}
	if (id == '') {
		parseError('No ID!');
		return;
	}
	var xhr = new XMLHttpRequest();

	try {
		xhr.onreadystatechange = function() {
			if (xhr.readyState != 4) return;
			parseSuccess(xhr.response);
		};

		xhr.onerror = function(error) {
			window.clearTimeout(abortTimerId);
			parseError("XMLHttpRequest Error: " + error.statusText);
		};

		xhr.open("GET", "https://www.ludomedia.it/ajax/misc/heart_beat.php?notifiche%5Bon%5D=1", true);
		xhr.setRequestHeader("M-Request-ID", id);
		xhr.setRequestHeader("X-Csrf-Token", token);
		xhr.timeout = 1000 * 3;
		xhr.ontimeout = function () { 
			parseError("XMLHttpRequest Timeout!"); 
		}
		xhr.send(null);
	} catch(e) {
		parseError("XMLHttpRequest Exception: " + e);
	}
}

function getToken(details) {
	token = undefined;
	id = undefined;
	for (var i = 0; i < details.requestHeaders.length; ++i) {
		if (details.requestHeaders[i].name === 'M-Request-ID') id = details.requestHeaders[i].value;
        else if (details.requestHeaders[i].name === 'X-Csrf-Token') token = details.requestHeaders[i].value;
	}
	if (token != undefined && id != undefined) {
		chrome.storage.local.set({'token': token, 'id': id}, function() {
			//console.log('[Ludomedia] Token & ID saved: ' + token + " & " + id);
		});
		chrome.webRequest.onBeforeSendHeaders.removeListener(getToken);
		getNotifications();
	} else {
		parseError("Unable to get token or id!");
	}
	return {requestHeaders: details.requestHeaders};
}


// Startup
chrome.browserAction.onClicked.addListener(function(){
	chrome.tabs.create({url: 'https://www.ludomedia.it/utente/notifiche'})
});

chrome.storage.local.get(['token','id'], function(result) {
		if (result.token) {
			token = result.token;
			// console.log('[Ludomedia] Token loaded: ' + token);
			if (result.id) {
				id = result.id;
				// console.log('[Ludomedia] ID loaded: ' + id);
				getNotifications();
			} else {
				parseError('No ID!');
			}
		} else {
			parseError('No Token!');
		}
});
	
chrome.alarms.onAlarm.addListener(getNotifications);

chrome.alarms.get('ludomedia', function (alarm) {
	if (alarm === undefined) {
		//console.log("[Ludomedia] Creating alarm...");
		chrome.alarms.create('ludomedia', {periodInMinutes:update_time});
	} else {
		//console.log("[Ludomedia] Alarm already running.");
	}
});

chrome.tabs.onUpdated.addListener(function(tabId, details, tab) {
    if (tab.url.indexOf('https://www.ludomedia.it/utente/notifiche') == 0 && details.status == 'complete') {
		chrome.browserAction.setBadgeText({text:"..."});
		chrome.webRequest.onBeforeSendHeaders.addListener(
			getToken,
			{urls: ["*://*.ludomedia.it/*"]},
			["requestHeaders"]
		);
    }
});

		
